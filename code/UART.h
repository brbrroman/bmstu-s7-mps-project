#define BAUD 9600
#define MYUBRR FOSC / 16 / BAUD - 1

void UartPrintln(char*);
void USART_Init(unsigned int);
void USART_Transmit(char);
void UartPrint(char*);
