#include <avr/io.h>

#include "matrixKeyboard.h"

void KeyboardInit() {
  KeyCol_DDR |= (1 << KeyColStart) * KeyColCNT;
  KeyCol_PORT |= (1 << KeyColStart) * KeyColCNT;
  KeyRow_DDR &= ~((1 << KeyRowStart) * KeyRowCNT);
  KeyRow_PORT &= ~((1 << KeyRowStart) * KeyRowCNT);
}

unsigned char which_key_pressed_unsafe() {
  for (unsigned char col = 0; col < KeyColCount; ++col) {
    KeyCol_PORT &= ~((1 << KeyColStart) * KeyColCNT);
    KeyCol_PORT |= (1 << (col + KeyColStart));
    // Если в строке хотя бы 1 кнопка нажата, то вычисляем конкретную
    if (KeyRow_PIN & ((1 << KeyRowStart) * KeyRowCNT)) {
      for (unsigned char row = 0; row < KeyRowCount; ++row) {
        if (KeyRow_PIN & (1 << (row + 1))) {
          KeyCol_PORT |= (1 << KeyColStart) * KeyColCNT;
          return (row * KeyColCount + col);
        }
      }
    }
  }
  KeyCol_PORT |= (1 << KeyColStart) * KeyColCNT;  // Восстановить 1 на колонках
  return (KeyColCNT * KeyRowCNT);  // Вернуть максимум если не нашли клавишу
}
