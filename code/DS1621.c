#include <util/delay.h>

#include "DS1621.h"
#include "I2C.h"

// Инициаизация датчика в состояние 0x03
void ds1621_init(unsigned char address) {
  ds1621_writeSingleByte(address, ACCESS_CONFIG, 0x03);
}

// Отправка 1 байта данных в датчик
void ds1621_writeSingleByte(unsigned char address, unsigned char command,
                            unsigned char data) {
  if (i2c_start() != 0) {
    i2c_stop();
    return;
  }

  address = (address % 0x08) << 1;  // Маскирование адреса
  if (i2c_start_wait(address | DS1621_W) != 0) {
    i2c_stop();
    return;
  }

  if (i2c_write(command) != 0) {
    i2c_stop();
    return;
  }

  if (i2c_write(data) != 0) {
    i2c_stop();
    return;
  }

  i2c_stop();
}

// Отправка команды в датчик
void ds1621_sendCommand(unsigned char address, unsigned char command) {
  if (i2c_start() != 0) {
    i2c_stop();
    return;
  }

  address = (address % 0x08) << 1;  // Маскирование адреса
  if (i2c_start_wait(address | DS1621_W) != 0) {
    i2c_stop();
    return;
  }

  if (i2c_write(command) != 0) {  // Успешная отправка команды?
    i2c_stop();
    return;
  }

  i2c_stop();
}

// Чтение байта данных из датчика
unsigned char ds1621_readValue(unsigned char address, unsigned char value) {
  if (i2c_start() != 0) {
    i2c_stop();
    return (0);
  }

  address = (address % 0x08) << 1;  // Маскирование адреса
  if (i2c_start_wait(address | DS1621_W) != 0) {
    i2c_stop();
    return (0);
  }

  if (i2c_write(value) != 0) {
    i2c_stop();
    return (0);
  }

  if (i2c_rep_start() != 0) {
    i2c_stop();
    return (0);
  }

  if (i2c_start_wait(address | DS1621_R) != 0) {
    i2c_stop();
    return (0);
  }

  unsigned char data = i2c_readNak();
  i2c_stop();
  return (data);
}

// Запрос температуры у датчика
signed char getTemperature(unsigned char address) {
  ds1621_sendCommand(address, START_CONVERT);
  _delay_ms(10);  // Время ожидания по даташиту
  return ds1621_readValue(address, READ_TEMP);
}
