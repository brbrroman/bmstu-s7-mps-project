#include <avr/io.h>

#include "SPI.h"

void SPI_Init(void) {
  SPI_DDR |= (1 << SPI_SS) | (1 << SPI_MOSI) | (1 << SPI_SCK);
  SPI_PORT |= (1 << SPI_SS);  // Установить "1" на линии SS
  SPCR = (1 << MSTR) | (1 << SPE);  // Режим мастер | Включить SPI
  SPSR = (1 << SPI2X);              // Удвоение частоты F=Fosc/2
}

void SPI_Send_byte(char data) {
  SPI_PORT &= ~(1 << SPI_SS);  // Установить "0" на линии SS
  SPDR = data;                 // Отправить байт
  while (!(SPSR & (1 << SPIF)))
    ;  // Дождаться окончания передачи
  SPI_PORT |= (1 << SPI_SS);  // Установить "1" на линии SS
}
