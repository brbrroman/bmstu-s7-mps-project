#include <avr/io.h>

#include "UART.h"

void USART_Init(unsigned int ubrr) {
  // Конфигурация скорости
  UBRRH = (unsigned char)(ubrr >> 8);
  UBRRL = (unsigned char)ubrr;
  // 8 бит данных, 1 стоп бит, без контроля четности
  UCSRC = (1 << URSEL) | (1 << UCSZ1) | (1 << UCSZ0);
  UCSRB = (1 << TXEN);
}

void USART_Transmit(char data) {
  while (!(UCSRA & (1 << UDRE)))
    ;  // Дождаться пустого буф.
  UDR = data;
}

void UartPrint(char* chars) {
  int i = 0;
  while (chars[i] != 0x00) {
    USART_Transmit(chars[i++]);
  }
}

void UartPrintln(char* chars) {
  UartPrint(chars);
  UartPrint("\n\r");
}
