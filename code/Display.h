#define DISP4DIG7SEG_PORT PORTB
#define DISP4DIG7SEG_DDR DDRB
#define DISP4DIG7SEG_RST PB1

//Инициализация адресных выходов для семисегментика
void displayInit();
void setAddress(unsigned int);
void displayPrintString(char*);
