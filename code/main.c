#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <util/delay.h>

#include "Display.h"
#include "I2C.h"
#include "SPI.h"
#include "UART.h"
#include "DS1621.h"
#include "main.h"
#include "matrixKeyboard.h"

#define DISPLAY_LOW "L %3dC"  // Формат вывода нижнего лимита
#define DISPLAY_HIGH "H %3dC"  // Формат вывода верхнего лимита
// Переменные для отображения на ССИ
char tempDisplay[] = " Load ", hideDisplay[];
enum { set_TL, set_TH, showTemp } stats = showTemp;  // Состояние
signed char temps[DS1621Count];  // Массив температур
unsigned char currentSensor = 0, prevSensor = 0, T2counter = 0;
signed char TH, TL;                               // Границы
signed char EEMEM EEMEM_TL = -10, EEMEM_TH = 50;  // Границы в EEPROM
bool hide = false;  // Для моргания дисплеем

// Обработчик прерывания 8-битного timer0
ISR(TIMER0_OVF_vect) {
  if (!hide) {
    displayPrintString(tempDisplay);
  } else {
    strncpy(hideDisplay, tempDisplay, 6);
    hideDisplay[2] = '_';
    hideDisplay[3] = '_';
    hideDisplay[4] = '_';
    displayPrintString(hideDisplay);
  }
}

// Обработчик прерывания 8-битного timer2 Моргание
ISR(TIMER2_OVF_vect) {
  T2counter = (T2counter++) % 8;
  if (T2counter == 0) {
    hide = !hide;
  }
}

// Обработчик прерывания 16-битного timer1
ISR(TIMER1_OVF_vect) {
  prevSensor = currentSensor;
  do {
    currentSensor = (currentSensor + 1) % DS1621Count;
  } while (((TL < temps[currentSensor]) && (temps[currentSensor] < TH)) &&
           (currentSensor != prevSensor));
  sprintf(tempDisplay, "%d %3dC", currentSensor + 1, temps[currentSensor]);
  UartPrintln(tempDisplay);
  Timer1CLR();
}

ISR(INT0_vect) {
  unsigned char key = which_key_pressed_unsafe();
  switch (key) {
    case 0:                      // Set TL
      TIMSK &= (~(1 << TOIE1));  // Отключение прерывания вывода
      TIMSK |= (1 << TOIE2);  // Включение моргания
      stats = set_TL;
      sprintf(tempDisplay, DISPLAY_LOW, TL);
      break;
    case 2:                      // Set TH
      TIMSK &= (~(1 << TOIE1));  // Отключение прерывания вывода
      TIMSK |= (1 << TOIE2);  // Включение моргания
      stats = set_TH;
      sprintf(tempDisplay, DISPLAY_HIGH, TH);
      break;
    case 1:  // temp+
    case 7:  // temp-
      switch (stats) {
        case set_TL:
          TL += (key + 1) % 8 - 1;
          sprintf(tempDisplay, DISPLAY_LOW, TL);
          break;
        case set_TH:
          TH += (key + 1) % 8 - 1;
          sprintf(tempDisplay, DISPLAY_HIGH, TH);
          break;
        default:
          break;
      }
      break;
    case 4:  // ok
      switch (stats) {
        case set_TL:
          eeprom_write_byte(&EEMEM_TL, TL);
          UartPrint("Low temp changed to:");
          sprintf(hideDisplay, "  %3dC", TL);
          UartPrintln(hideDisplay);
          break;
        case set_TH:
          eeprom_write_byte(&EEMEM_TH, TH);
          UartPrint("High temp changed to:");
          sprintf(hideDisplay, "  %3dC", TH);
          UartPrintln(hideDisplay);
          break;
        default:
          break;
      }
      stats = showTemp;
      Timer1CLR();
      TIMSK &= ~(1 << TOIE2);
      hide = false;
      TIMSK |= (1 << TOIE1);
    case 3:  // prev
    case 5:  // next
      if (stats == showTemp) {
        currentSensor = (currentSensor + (DS1621Count - 4) + key) % DS1621Count;
        sprintf(tempDisplay, "%d %3dC", currentSensor + 1,
                temps[currentSensor]);
        Timer1CLR();
        UartPrintln(tempDisplay);
      }
      break;
  }
}

int main() {
  for (unsigned char sensor = 0; sensor < DS1621Count; ++sensor) {
    ds1621_init(sensor);
    ds1621_sendCommand(sensor, START_CONVERT);
  }

  TH = eeprom_read_byte(&EEMEM_TH);
  TL = eeprom_read_byte(&EEMEM_TL);

  displayInit();
  KeyboardInit();
  USART_Init(MYUBRR);
  i2c_init();

  TIMSK |= (1 << TOIE0) | (1 << TOIE1);
  Timer0Config();  // Таймер обновления семисегментого индикатора
  Timer1Config();  // Таймер для смены датчика
  Timer1CLR();
  Timer2Config();  // Таймер для моргания дисплея

  GICR |= (1 << INT0);  // Разрешаем прерывание INT0
  MCUCR |= (1 << ISC00) |
           (1 << ISC01);  // Генерация сигнала по возрастающему фронту PD2

  sei();  // Разрешить прерывания

  while (1) {
    switch (stats) {
      case showTemp:
        for (unsigned char sensor = 0; sensor < DS1621Count; ++sensor) {
          temps[sensor] = getTemperature(sensor);
        }
        _delay_ms(1000);
        break;
      default:
        sleep_enable();
        sleep_cpu();
        break;
    }
  }
  return 0;
}
