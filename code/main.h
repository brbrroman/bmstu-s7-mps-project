#define FOSC 4000000UL                        // Clock Speed
#define Timer0Config() (TCCR0 = (1 << CS02))  // Примерно 60Гц
#define Timer1Config() (TCCR1B = (1 << CS12) | (1 << CS10))  // Меньше 1Гц
#define Timer1CLR() (TCNT1 = 32768);
#define Timer2Config() (TCCR2 = (1 << CS22) | (1 << CS21) | (1 << CS20))
#define DS1621Count 7
