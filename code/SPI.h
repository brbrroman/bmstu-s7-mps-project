#define SPI_PORT PORTB
#define SPI_DDR DDRB
#define SPI_MOSI PB3
#define SPI_SS PB2
#define SPI_SCK PB5

void SPI_Init(void);
void SPI_Send_byte(char);
