#include <util/twi.h>

#include "I2C.h"
#include "math.h"

void i2c_init(void) { TWBR = BITRATE(TWSR = 0x00); };

unsigned char i2c_start(void) {
  TWCR = (1 << TWINT) | (1 << TWSTA) |
         (1 << TWEN);  // Прерывание, Старт, Включение
  while (!(TWCR & (1 << TWINT)))
    ;
  return ((TWSR & 0xF8) == TW_START) ? 0 : 1;  // Старт произошел?
}

unsigned char i2c_rep_start(void) {
  TWCR = (1 << TWINT) | (1 << TWSTA) |
         (1 << TWEN);  // Прерывание, Старт, Включение
  while (!(TWCR & (1 << TWINT)))
    ;
  return ((TWSR & 0xF8) == TW_REP_START) ? 0 : 1;
}

unsigned char i2c_start_wait(unsigned char address) {
  unsigned char STATUS;
  STATUS = ((address & 0x01) == 0) ? TW_MT_SLA_ACK : TW_MR_SLA_ACK;
  TWDR = address;
  TWCR = (1 << TWINT) | (1 << TWEN);
  while (!(TWCR & (1 << TWINT)))
    ;
  return ((TWSR & 0xF8) == STATUS) ? 0 : 1;
}

unsigned char i2c_write(unsigned char data) {
  TWDR = data;
  TWCR = (1 << TWINT) | (1 << TWEN);
  while (!(TWCR & (1 << TWINT)))
    ;
  return ((TWSR & 0xF8) != TW_MT_DATA_ACK) ? 1 : 0;
}

unsigned char i2c_readAck(void) {
  TWCR = (1 << TWEA) | (1 << TWINT) | (1 << TWEN);
  while (!(TWCR & (1 << TWINT)))
    ;
  return TWDR;
}

unsigned char i2c_readNak(void) {
  TWCR = (1 << TWINT) | (1 << TWEN);
  while (!(TWCR & (1 << TWINT)))
    ;
  return TWDR;
}

void i2c_stop(void) {
  TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);
  while (TWCR & (1 << TWSTO))
    ;
}
