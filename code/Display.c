#include <avr/io.h>
#include <util/delay.h>

#include "Display.h"
#include "HC595.h"

//Инициализация адресных выходов для семисегментика
void displayInit() {
  DISP4DIG7SEG_DDR |= 1 << DISP4DIG7SEG_RST;  // Разрешаем запись в пин сброса
  HC595Init();  // Инициализируем сдвиговый регистр
}

void displayPrintString(char* chars) {
  HC595OEHigh();  // Снятие сигнала с выхода HC595
  DISP4DIG7SEG_PORT |= 1 << DISP4DIG7SEG_RST;  // сброс разряда дисплея
  DISP4DIG7SEG_PORT &= ~(1 << DISP4DIG7SEG_RST);  // окончание сброса
  HC595_sendSymbol(chars[0]);
  HC595OELow();  // Установка сигнала на выходах HC595
  _delay_ms(1);
  for (unsigned int digit_number = 1; digit_number < 6; ++digit_number) {
    HC595OEHigh();  // Снятие сигнала с выхода HC595
    HC595_sendSymbol(chars[digit_number]);
    HC595OELow();  // Установка сигнала на выходах HC595
    _delay_ms(1);  // задержка для протеуса
  }
}
