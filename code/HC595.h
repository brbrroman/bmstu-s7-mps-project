#define HC595_PORT PORTB
#define HC595_DDR DDRB
#define HC595_NOT_OE_POS PB0  // Store pin (Not OE) pin location

#define HC595OEHigh() (HC595_PORT |= (1 << HC595_NOT_OE_POS))
#define HC595OELow() (HC595_PORT &= (~(1 << HC595_NOT_OE_POS)))

void HC595Init();
void HC595_sendSymbol(unsigned int);
