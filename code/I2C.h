#define FOSC 4000000UL  // Clock Speed
#define SCL_CLK 250000L

#define BITRATE(TWSR) \
  ((FOSC / SCL_CLK) - 16) / (2 * pow(4, (TWSR & ((1 << TWPS0) | (1 << TWPS1)))))
// SCL_CLK = F_cpu/(16 + 2* TWBR* 4^TWPS)

void i2c_init(void);
unsigned char i2c_start(void);
unsigned char i2c_rep_start(void);
unsigned char i2c_start_wait(unsigned char);
unsigned char i2c_write(unsigned char);
unsigned char i2c_readAck(void);
unsigned char i2c_readNak(void);
void i2c_stop(void);
