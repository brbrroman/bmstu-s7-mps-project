#define KeyRow_PORT PORTC
#define KeyRow_DDR DDRC
#define KeyRow_PIN PINC
#define KeyRowStart PC1
#define KeyRowCount 3
#define KeyCol_PORT PORTD
#define KeyCol_DDR DDRD
#define KeyColStart PD5
#define KeyColCount 3
#define KeyRowCNT ((1 << KeyRowCount) - 1)
#define KeyColCNT ((1 << KeyColCount) - 1)

void KeyboardInit(void);
unsigned char which_key_pressed_unsafe();
